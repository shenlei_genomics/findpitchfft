// FindPitchByFFT.cpp : Defines the entry point for the console application.
//
#pragma once
#include "stdafx.h"
#include "Tools.h"
#include <opencv/cv.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <iterator>
#include <io.h>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

//#define WRITE_FREQ_SPETRUM

using namespace cv;
using namespace std;

//const int TH = 10000;
Mat rho;
int width;

int genCenterImg(Mat& I, Mat& img_center)
{
	if (I.empty() )
	{
		return -1;
	}
	
	width = std::min(I.cols >> 1, I.rows >> 1);
	Point start = Point(I.cols >> 2, I.rows >> 2);
	Mat center = I(Range(start.y, start.y + width), Range(start.x, start.x + width));
	center.copyTo(img_center);
	return 0;
}

void genPolar(const int cy, const int cx, Mat& mag)
{
	Mat x = Mat::zeros(cy * 2, cx * 2, CV_32F);
	Mat y = Mat::zeros(cy * 2, cx * 2, CV_32F);
	for (int r = -cy; r < cy; ++r)
	{
		for (int c = -cx; c < cx; ++c)
		{
			x.at<float>(r + cy, c + cx) = static_cast<float>(c);
			y.at<float>(r + cy, c + cx) = static_cast<float>(r);
		}
	}
	Mat angle;
	cartToPolar(x, y, mag, angle);
}

/*@belief: init global params includes bright contaminant Threshold, low_freq_beg, high_freq_beg, rho
@param   I   pick-up the first img as input to calculate these params
*/
void initGlobalParams()
{	
	genPolar(width >> 1, width >> 1, rho);	
}


void getFreqSpectrum(const Mat& rho, const Mat& power, vector<float>& freq_spectrum)
{
	Scalar s = sum(power);
	float power_spt_sum = s(0);
	freq_spectrum.clear();
	freq_spectrum.assign(ceil(sqrt(power.rows * power.rows + power.cols*power.cols) / 2), 0.0f);
	for (size_t y = 0; y < power.rows; ++y)
	{
		for (size_t x = 0; x < power.cols; x++)
		{
			size_t index = static_cast<size_t>(floor(rho.at<float>(y, x)));
			//cout << "index = " << index << "\tx = " << x << "\ty = " << y << "\trho = " << rho.at<float>(y,x) << "\n";
			freq_spectrum[index] += power.at<float>(y, x);
		}
	}
	for (vector<float>::iterator it = freq_spectrum.begin(); it != freq_spectrum.end(); ++it)
	{
		*it /= power_spt_sum;
	}
}

int powerSpectrumD(const Mat& I, vector<float>& freq_spectrum , const string& img_fn)//vector<float>& freq_spectrum)
{
	if (I.empty())
		return -1;

	Mat padded;                            //expand input image to optimal size
	int m = getOptimalDFTSize(I.rows);
	int n = getOptimalDFTSize(I.cols); // on the border add zero values
	copyMakeBorder(I, padded, 0, m - I.rows, 0, n - I.cols, BORDER_CONSTANT, Scalar::all(0));
	Mat planes[] = { Mat_<float>(padded), Mat::zeros(padded.size(), CV_32F) };
	Mat complexI, powerSpt;
	merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

	dft(complexI, complexI);            // this way the result may fit in the source matrix

										// compute the magnitude and switch to logarithmic scale
										// => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
	split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
	magnitude(planes[0], planes[1], planes[0]);// planes[0] = magnitude
	Mat magI;
	magI = planes[0];
	powerSpt = magI.mul(magI);

	// crop the spectrum, if it has an odd number of rows or columns
	powerSpt = powerSpt(Rect(0, 0, powerSpt.cols & -2, powerSpt.rows & -2));

	// rearrange the quadrants of Fourier image  so that the origin is at the image center
	int cx = powerSpt.cols / 2;
	int cy = powerSpt.rows / 2;

	Mat q0(powerSpt, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
	Mat q1(powerSpt, Rect(cx, 0, cx, cy));  // Top-Right
	Mat q2(powerSpt, Rect(0, cy, cx, cy));  // Bottom-Left
	Mat q3(powerSpt, Rect(cx, cy, cx, cy)); // Bottom-Right

	Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
	q0.copyTo(tmp);
	q3.copyTo(q0);
	tmp.copyTo(q3);

	q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
	q2.copyTo(q1);
	tmp.copyTo(q2);

	
	getFreqSpectrum(rho, powerSpt, freq_spectrum);

#ifdef WRITE_FREQ_SPETRUM	
	ofstream fs("./high_freq_spectrum.txt", ios::app);
	if (fs)
	{
		fs << img_fn << "\t";
		copy(begin(freq_spectrum) + 500, end(freq_spectrum), std::ostream_iterator<float>(fs, "\t"));
		fs << "\n";
	}
	fs.close();
#endif
	return 0;
}

vector<size_t> pitchfreqs = { 680,757,909,947,784,708 };//800nm, 720nm, 600nm, 576nm, 480nm, 450nm
const size_t search_radius = 4;
int findpitch(const vector<float>& fftmag, const vector<size_t>& pitchfreqs)
{	
	double maxmag = 0.0;
	int max_freq_index = -1;
	vector<float>::const_iterator itb = fftmag.cbegin();
	for (int i = 0 ; i < pitchfreqs.size(); ++i)
	{
		double val = *max_element(itb + pitchfreqs[i] - search_radius, itb + pitchfreqs[i] + search_radius);
		if (val > maxmag)
		{
			maxmag = val;
			max_freq_index = i;
		}
	}	
	if (maxmag < 0.0003)
		return -1;
	else
		return max_freq_index;
}

bool ifinit = false;

int getPitchIndex(const string& imgfn)
{
	Mat I = imread(imgfn, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
	if (I.empty())
	{
		return -1;
	}
	Mat tmp_m, tmp_sd;
	meanStdDev(I, tmp_m, tmp_sd);
	double m = tmp_m.at<double>(0, 0);
	double std = tmp_sd.at<double>(0, 0);
	if (m < 500 && std < 200)
	{
		return -1;
	}
	Mat img_center;
	genCenterImg(I, img_center);
	if (!ifinit)
	{
		genPolar(width >> 1, width >> 1, rho);
		ifinit = true;
	}
	vector<float> fftmag;
	powerSpectrumD(img_center, fftmag, imgfn);
	return findpitch(fftmag, pitchfreqs);	
}


int main(int argc, char ** argv)
{
	if (argc < 2)
	{		
		return -1;
	}	
	const string imgfn = argv[1];
	/*const string filepath = argv[1];
	vector<string> files;
	getFiles(filepath,"tif", files);
	bool ifinit = false;
	for (auto it = files.cbegin(); it != files.cend(); ++it)
	{
		Mat I = imread(*it, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
		Mat tmp_m, tmp_sd;
		meanStdDev(I, tmp_m, tmp_sd);
		double m = tmp_m.at<double>(0, 0);
		double std = tmp_sd.at<double>(0, 0);
		if (m < 500 && std < 200)
		{
			continue;
		}
		Mat img_center;
		genCenterImg(I, img_center);
		if (!ifinit)
		{
			genPolar(width>>1, width>>1, rho);
			ifinit = true;
		}
		vector<float> fftmag;
		powerSpectrumD(img_center, fftmag, *it);
		int pitch_index = findpitch(fftmag, pitchfreqs);
		string pitchinfo[] = { "800nm","720nm","600nm","576nm","480nm","450nm" };
		if (pitch_index > -1)
		{
			std::cout << *it << "\t" << pitchinfo[pitch_index] << std::endl;
		}
		else
		{
			std::cout << *it << std::endl;
		}
		
	}*/


    return getPitchIndex(imgfn);
	
}

